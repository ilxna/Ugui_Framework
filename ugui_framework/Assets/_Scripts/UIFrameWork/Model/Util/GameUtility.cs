﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace TinyFrameWork
{
	public class GameUtility
	{
	/// <summary>
	/// 通过名字找到子物体的transform
	/// </summary>
	/// <param name="_target"></param>
	/// <param name="_childName"></param>
	/// <returns></returns>
	public static Transform FindDeepChild(GameObject _target,string _childName)
		{
			Transform resultTrs = null;
			resultTrs = _target.transform.Find(_childName);
			if (resultTrs==null)
			{
				foreach (Transform trs in _target.transform)
				{
					resultTrs = GameUtility.FindDeepChild(trs.gameObject, _childName);
					if (resultTrs!=null)
					{
						return resultTrs;
					}
				}
			}

			return resultTrs;

		}
/// <summary>
/// 找到子物体的组件
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="_target"></param>
/// <param name="_childName"></param>
/// <returns></returns>
		public static T FindDeepChild<T>(GameObject _target,string _childName) where T:Component
		{
			Transform resultTrs = GameUtility.FindDeepChild(_target, _childName);

			if (resultTrs!=null)
			{
				return resultTrs.gameObject.GetComponent<T>();
			}
			return (T)((object)null);
		}
/// <summary>
/// 添加子节点
/// </summary>
/// <param name="target"></param>
/// <param name="child"></param>
	public static void AddChildToTarget(Transform target,Transform child)
		{
			child.parent = target;
			child.localScale = Vector3.one;
			child.localPosition = Vector3.zero;

			child.localEulerAngles = Vector3.zero;

			ChangeChildLayer(child, target.gameObject.layer);
		} 
	/// <summary>
	/// 修改子节点的layer
	/// </summary>
	/// <param name="t"></param>
	/// <param name="layer"></param>
	public static void ChangeChildLayer(Transform trs,int layer)
		{
			trs.gameObject.layer = layer;
			for (int i = 0; i < trs.childCount; ++i)
			{
				Transform child = trs.GetChild(i);

				child.gameObject.layer = layer;

				ChangeChildLayer(child, layer);
			}
		}
	}
}